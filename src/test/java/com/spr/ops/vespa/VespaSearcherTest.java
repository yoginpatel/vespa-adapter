package com.spr.ops.vespa;

import com.google.gson.Gson;
import com.spr.shifu.listening.firehose.TweetReport;
import com.spr.shifu.request.Request;
import com.yahoo.component.ComponentId;
import com.yahoo.search.Query;
import com.yahoo.search.Result;
import com.yahoo.search.cache.QrBinaryCacheConfig;
import com.yahoo.search.cache.QrBinaryCacheRegionConfig;
import com.yahoo.search.federation.ProviderConfig;
import com.yahoo.search.federation.vespa.VespaSearcher;
import com.yahoo.search.grouping.vespa.GroupingExecutor;
import com.yahoo.search.searchchain.Execution;
import com.yahoo.search.searchchain.SearchChain;
import com.yahoo.statistics.Statistics;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Date 10/10/17
 * Time 11:55 AM
 *
 * @author yogin
 */
public class VespaSearcherTest {

    private boolean basic = false;

    private VespaSearcher searcher;
    private VespaQueryAdapter adapter;
    private Request sampleRequest;

    private GroupingExecutor groupingExecutor;

    @Before
    public void setup() throws IOException {
        if (basic) {
            searcher = new VespaSearcher("component", "localhost", 7777, "/search/");
        } else {
            ProviderConfig.Builder builder = new ProviderConfig.Builder();
            builder.node(new ProviderConfig.Node.Builder().host("localhost").port(7788));
            builder.path("/search/").readTimeout(100*1000).connectionTimeout(5*1000).connectionPoolTimeout(5*1000);
            builder.queryType(ProviderConfig.QueryType.YQL);
            ProviderConfig providerConfig = new ProviderConfig(builder);

            searcher = new VespaSearcher(new ComponentId("component"), providerConfig, new QrBinaryCacheConfig(new QrBinaryCacheConfig.Builder()), new QrBinaryCacheRegionConfig(new QrBinaryCacheRegionConfig.Builder()), Statistics.nullImplementation);
        }

        groupingExecutor = new GroupingExecutor(new ComponentId("groupingExecutor"));

        adapter = new VespaQueryAdapterImpl(TweetReport.getInstance());
        sampleRequest = new Gson().fromJson(toString("/sampleSearchRequest.json"), Request.class);
    }

    @Test
    public void testSearch() {
        Query query = adapter.adapt(sampleRequest);
        //Result result = searcher.search(query, new Execution());

        Result result = groupingExecutor.search(query, new Execution(new SearchChain(new ComponentId("searchChaing"), searcher), Execution.Context.createContextStub()));
        System.out.println(result);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String toString(String fileName) throws IOException {
        return IOUtils.toString(this.getClass().getResourceAsStream(fileName), "UTF-8");
    }
}
