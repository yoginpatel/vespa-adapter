package com.spr.ops.vespa;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.spr.shifu.listening.firehose.TweetReport;
import com.spr.shifu.request.Filter;
import com.spr.shifu.request.Request;
import com.yahoo.search.Query;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Date 10/10/17
 * Time 11:54 AM
 *
 * @author yogin
 */
public class VespaQueryAdapterTest {

    private VespaQueryAdapter adapter;
    private List<Request> sampleRequests;
    private Filter commonFilter;

    @Before
    public void setup() throws IOException {
        adapter = new VespaQueryAdapterImpl(TweetReport.getInstance());

        Type typeToken = new TypeToken<List<Request>>() {}.getType();
        sampleRequests = new Gson().fromJson(toString("/sampleRequests.json"), typeToken);
        commonFilter = new Filter(Filter.FilterType.IN, "sddocname").addValue("tweet");
    }

    @Test
    public void adapt() throws Exception {
        for (Request sampleRequest : sampleRequests) {
            if (CollectionUtils.isEmpty(sampleRequest.getFilters())) {
                sampleRequest.addFilter(commonFilter); //yql needs at-least one filter for aggregation
            }

            Query query = adapter.adapt(sampleRequest);
            System.out.println(query.yqlRepresentation());
        }
    }

    private String toString(String fileName) throws IOException {
        return IOUtils.toString(this.getClass().getResourceAsStream(fileName), "UTF-8");
    }
}