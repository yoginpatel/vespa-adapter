package com.spr.shifu.request;

import com.google.common.collect.Maps;

import java.util.Map;

public class BatchRequest {

    private String firstRequestKey;
    private boolean pivotRequest;
    private Map<String, Request> requests;

    public String getFirstRequestKey() {
        return firstRequestKey;
    }

    public void setFirstRequestKey(String firstRequestKey) {
        this.firstRequestKey = firstRequestKey;
    }

    public boolean isPivotRequest() {
        return pivotRequest;
    }

    public void setPivotRequest(boolean pivotRequest) {
        this.pivotRequest = pivotRequest;
    }

    public Map<String, Request> getRequests() {
        return requests;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setRequests(Map<String, Request> requests) {
        this.requests = requests;
    }

    public BatchRequest addRequest(String name, Request request) {
        if (requests == null) {
            requests = Maps.newHashMap();
        }
        requests.put(name, request);
        return this;
    }

    public boolean isEmpty() {
        return requests == null || requests.isEmpty();
    }
}
