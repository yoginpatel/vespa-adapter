package com.spr.shifu.request;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

public class Group {

    public enum GroupType {
        RANGE, HISTOGRAM, DATE_HISTOGRAM, FIELD, TIME_OF_DAY, DAY_OF_WEEK, MONTH_OF_YEAR, WORD_CLOUD, FILTER, MULTI_FIELD
    }

    private String key;

    private String field;

    private GroupType groupType;

    private Map<String, Object> details;

    private Sort sort;

    private Page page;

    private List<Projection> projections;

    private List<Filter> filters;

    private List<Group> childrenGroupBys;

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public List<Projection> getProjections() {
        return projections;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setProjections(List<Projection> projections) {
        this.projections = projections;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getField() {
        return field;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setField(String field) {
        this.field = field;
    }

    public GroupType getGroupType() {
        return groupType;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setGroupType(GroupType groupType) {
        this.groupType = groupType;
    }

    public Map<String, Object> getDetails() {
        return details;
    }

    public void setDetails(Map<String, Object> details) {
        this.details = details;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }


    public List<Group> getChildrenGroupBys() {
        return childrenGroupBys;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setChildrenGroupBys(List<Group> childrenGroupBys) {
        this.childrenGroupBys = childrenGroupBys;
    }

    public Group key(String key) {
        this.key = key;
        return this;
    }

    public Group field(String field) {
        this.field = field;
        return this;
    }

    public Group groupType(GroupType groupType) {
        this.groupType = groupType;
        return this;
    }

    @SuppressWarnings("UnusedDeclaration")
    public Group addDetail(String key, Object value) {
        if (details == null) {
            details = Maps.newHashMap();
        }
        details.put(key, value);
        return this;
    }

    public Group sort(Sort sort) {
        this.sort = sort;
        return this;
    }

    public Group page(Page page) {
        this.page = page;
        return this;
    }

    public Group addProjection(Projection projection) {
        if (projections == null) {
            projections = Lists.newArrayList();
        }
        projections.add(projection);
        return this;
    }

    public Group addFilter(Filter filter) {
        if (filters == null) {
            filters = Lists.newArrayList();
        }
        filters.add(filter);
        return this;
    }

    public Group addChildGroupBy(Group group) {
        if (group != null) {
            if (childrenGroupBys == null) {
                childrenGroupBys = Lists.newArrayList();
            }
            childrenGroupBys.add(group);
        }
        return this;
    }
}
