package com.spr.shifu.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * User: rajesh
 * Date: 26/02/14
 * Time: 6:37 PM
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Page implements Serializable {

    private int page;
    private int size;
    private int skip;

    public Page() {}

    public Page(int page, int size) {
        this.page = page;
        this.size = size;
    }

    public Page(int page, int size, int skip) {
        this.page = page;
        this.size = size;
        this.skip = skip;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public int start() {
        if (skip > 0) {
            return skip;
        } else {
            return page * size;
        }
    }

    public int end() {
        if (skip > 0) {
            return skip + size;
        } else {
            return (page + 1) * size;
        }
    }

    @Override
    public String toString() {
        return "Page{" +
               "page=" + page +
               ", size=" + size +
               '}';
    }
}
