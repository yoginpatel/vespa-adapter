package com.spr.shifu.request;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by saiaravindbv on 30/05/17.
 */
public class Query implements Serializable {

    public enum QueryType {
        AND,
        OR,
        NOT,

        BOOL,
        DIS_MAX,
        FUNCTION_SCORE,

        MATCH,
        MATCH_PHRASE,
        QUERY_STRING,
        TERMS
    }

    private QueryType queryType;

    private String field;

    private String queryText;

    private float boost = 1.0F;

    private List<Query> queries;

    private List<Filter> filters;

    private Map<String, Object> details = Maps.newHashMap();

    public Query() {
    }

    public Query(QueryType queryType, String field) {
        this.queryType = queryType;
        this.field = field;
    }

    public QueryType getQueryType() {
        return queryType;
    }

    public void setQueryType(QueryType queryType) {
        this.queryType = queryType;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getQueryText() {
        return queryText;
    }

    public void setQueryText(String queryText) {
        this.queryText = queryText;
    }

    public float getBoost() {
        return boost;
    }

    public void setBoost(float boost) {
        this.boost = boost;
    }

    public List<Query> getQueries() {
        return queries;
    }

    public void setQueries(List<Query> queries) {
        this.queries = queries;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public Map<String, Object> getDetails() {
        return details;
    }

    public void setDetails(Map<String, Object> details) {
        this.details = details;
    }

    public Query field(String field) {
        this.field = field;
        return this;
    }

    public Query queryType(QueryType queryType) {
        this.queryType = queryType;
        return this;
    }

    public Query queryText(String queryText) {
        this.queryText = queryText;
        return this;
    }

    public Query boost(float boost) {
        this.boost = boost;
        return this;
    }

    public Query addQuery(Query query) {
        if (queries == null) {
            queries = Lists.newArrayList();
        }
        queries.add(query);
        return this;
    }

    public Query addFilter(Filter filter) {
        if (filters == null) {
            filters = Lists.newArrayList();
        }
        filters.add(filter);
        return this;
    }

}
