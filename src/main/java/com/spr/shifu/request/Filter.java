package com.spr.shifu.request;


import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Filter implements Serializable {

    static {
    }

    public static final String FIELD_FIELD = "field";
    public static final String FIELD_VALUES = "values";

    public enum FilterType {
        AND("And", true), OR("Or", true), NOT("Not", true), IN("Containing"), GT("Greater than"), GTE("Greater than or Equal to"), LT("Less than"),
        LTE("Less than or Equal to"), NIN("Not Containing"), BETWEEN("Between"), STARTS_WITH("Starts With"), CONTAINS("Contains"),
        DOES_NOT_CONTAIN("Does not contain"), EQUALS("Equals"), FILTER, EXISTS("Exists"), REGEX("Regex"), LIMIT, SEARCH, MISSING, ADHOC_SEARCH,
        EXPRESSION, GEO_DISTANCE, NOT_EQUALS, ADVANCED_QUERY;

        private String displayName;
        private boolean isCompoundFilter;

        FilterType() {
        }

        FilterType(String displayName) {
            this(displayName, false);
        }

        FilterType(String displayName, boolean isCompoundFilter) {
            this.displayName = displayName;
            this.isCompoundFilter = isCompoundFilter;
        }

        public String getDisplayName() {
            return displayName;
        }

        public boolean isCompoundFilter() {
            return isCompoundFilter;
        }
    }



    private FilterType filterType;

    private String field;

    private List<Object> values;

    private Map<String, Object> details = Maps.newHashMap();

    private List<Filter> filters;

    public Filter() {
    }

    public Filter(Filter filter) {
        this.filterType = filter.filterType;
        this.field = filter.field;
        this.values = filter.values;
        this.details = filter.details;
        this.filters = filter.filters;
    }

    public Filter(FilterType filterType, String field, List<Object> values) {
        this.filterType = filterType;
        this.field = field;
        this.values = values;
    }

    public Filter(FilterType filterType, String field) {
        this.filterType = filterType;
        this.field = field;
    }

    public FilterType getFilterType() {
        return filterType;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setFilterType(FilterType filterType) {
        this.filterType = filterType;
    }

    public String getField() {
        return field;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setField(String field) {
        this.field = field;
    }

    public List<Object> getValues() {
        return values;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setValues(List<Object> values) {
        this.values = values;
    }

    public Map<String, Object> getDetails() {
        return details;
    }

    public void setDetails(Map<String, Object> details) {
        this.details = details;
    }

    public Object getDetail(String key) {
        if (this.details == null) {
            return null;
        }
        return this.details.get(key);
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public Filter addFilter(Filter filter) {
        if (CollectionUtils.isEmpty(this.filters)) {
            this.filters = Lists.newArrayList();
        }
        this.filters.add(filter);
        return this;
    }

    public Filter addFilters(List<Filter> filters) {
        if (CollectionUtils.isEmpty(filters)) {
            return this;
        }

        if (CollectionUtils.isEmpty(this.filters)) {
            this.filters = Lists.newArrayList();
        }
        this.filters.addAll(filters);
        return this;
    }

    public Filter addDetail(String key, Object value) {
        if (details == null) {
            details = Maps.newHashMap();
        }
        details.put(key, value);
        return this;
    }

    @SuppressWarnings("unused")
    public Filter removeDetail(String key) {
        if (details != null) {
            details.remove(key);
        }
        return this;
    }

    public Filter details(Map<String, Object> detailsToAppend) {
        if (MapUtils.isNotEmpty(detailsToAppend)) {
            if (this.details == null) {
                this.details = Maps.newHashMap();
            }
            this.details.putAll(detailsToAppend);
        }
        return this;
    }

    public Filter field(String field) {
        this.field = field;
        return this;
    }

    public Filter filterType(FilterType filterType) {
        this.filterType = filterType;
        return this;
    }

    public Filter addValue(Object value) {
        if (values == null) {
            values = Lists.newArrayList();
        }
        values.add(value);
        return this;
    }

    public Filter values(List<Object> values) {
        this.values = values;
        return this;
    }

    @Override
    public String toString() {
        return "Filter{" +
               "filterType=" + filterType +
               ", field='" + field + '\'' +
               ", values=" + values +
               ", details=" + details +
               '}';
    }

    @Override
    public boolean equals(Object obj) {
        final boolean isEquals;
        if (this == obj) {
            isEquals = true;
        } else if (obj instanceof Filter) {
            Filter other = (Filter) obj;
            final EqualsBuilder equalsBuilder = new EqualsBuilder();
            equalsBuilder.append(this.field, other.field).append(this.filterType, other.filterType).append(this.values, other.values)
                         .append(this.details, other.details);
            isEquals = equalsBuilder.isEquals();
        } else {
            isEquals = false;
        }
        return isEquals;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(this.field).append(this.filterType).append(this.values).append(this.details).toHashCode();
    }

    @SuppressWarnings("CloneDoesntCallSuperClone")
    @Override
    public Filter clone() {
        Filter filter = new Filter(this.filterType, this.field);
        if (MapUtils.isNotEmpty(this.details)) {
            filter.setDetails(Maps.newHashMap(this.details));
        }
        if (CollectionUtils.isNotEmpty(this.values)) {
            filter.setValues(Lists.newArrayList(this.values));
        }
        return filter;
    }
}
