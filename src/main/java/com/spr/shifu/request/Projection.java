package com.spr.shifu.request;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;

public class Projection {



    private String key;
    private String measurement;
    private AggregateFunction aggregateFunction = AggregateFunction.SUM;
    private Map<String, Object> details;
    private List<Filter> filters;

    public Projection() {

    }

    public Projection(Projection projection) {
        addDetails(projection.details).aggregateFunction(projection.aggregateFunction).key(projection.key).measurement(projection.measurement);
    }

    public String getKey() {

        return key;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setKey(String key) {
        this.key = key;
    }

    public String getMeasurement() {
        return measurement;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public AggregateFunction getAggregateFunction() {
        return aggregateFunction;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setAggregateFunction(AggregateFunction aggregateFunction) {
        this.aggregateFunction = aggregateFunction;
    }

    @SuppressWarnings("UnusedDeclaration")
    public Map<String, Object> getDetails() {
        return details;
    }

    public void setDetails(Map<String, Object> details) {
        this.details = details;
    }

    public Projection key(String key) {
        this.key = key;
        return this;
    }

    public Projection measurement(String measurement) {
        this.measurement = measurement;
        return this;
    }

    public Projection aggregateFunction(AggregateFunction aggregateFunction) {
        this.aggregateFunction = aggregateFunction;
        return this;
    }

    public Projection addFilter(Filter filter) {
        if (filters == null) {
            filters = Lists.newArrayList();
        }
        filters.add(filter);
        return this;
    }

    public Projection addFilters(List<Filter> filters) {
        if (filters == null) {
            return this;
        }
        if (this.filters == null) {
            this.filters = Lists.newArrayList();
        }
        this.filters.addAll(filters);
        return this;
    }

    public Projection addDetail(String key, Object value) {
        if (details == null) {
            details = Maps.newHashMap();
        }
        details.put(key, value);
        return this;
    }

    public Projection addDetails(Map<String, Object> details) {
        if (details == null) {
            return this;
        }

        if (this.details == null) {
            this.details = Maps.newHashMap();
        }
        this.details.putAll(details);
        return this;
    }

    @Override
    public String toString() {
        return "Projection{" +
               "key='" + key + '\'' +
               ", measurement='" + measurement + '\'' +
               ", aggregateFunction=" + aggregateFunction +
               ", details=" + details +
               '}';
    }

    public enum AggregateFunction {
        SUM, AVG, MIN, MAX, COUNT, STATS, EXTERNAL_VALUE, CARDINALITY, TOP_HITS, PERCENTILE
    }
}
