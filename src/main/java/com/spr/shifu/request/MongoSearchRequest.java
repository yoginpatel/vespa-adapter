package com.spr.shifu.request;

import java.util.Map;

/**
 * Container for all the fields needed to perform a search with paging
 * against MongoDB. Typically useful for implementations.
 *
 * @author mikegarrett
 * @since 02/19/2016
 */
public class MongoSearchRequest {

    private String queryString;
    private String queryField;
    private Page page;
    private Sort sort;
    private long partnerId;
    private Map<String, String> additionalQueryFields;
    private String assetType;

    public MongoSearchRequest(String queryString, String queryField, Page page, Sort sort, long partnerId) {
        this.queryString = queryString;
        this.queryField = queryField;
        this.page = page;
        this.partnerId = partnerId;
        this.sort = sort;
    }

    public MongoSearchRequest(String queryString, String queryField, Page page, Sort sort, long partnerId, String assetType) {
        this.queryString = queryString;
        this.queryField = queryField;
        this.page = page;
        this.partnerId = partnerId;
        this.sort = sort;
        this.assetType = assetType;
    }

    public MongoSearchRequest( String queryString, String queryField, Page page, Sort sort, long partnerId,
                               Map<String, String> additionalQueryFields ) {
        this.queryString = queryString;
        this.queryField = queryField;
        this.page = page;
        this.sort = sort;
        this.partnerId = partnerId;
        this.additionalQueryFields = additionalQueryFields;
    }

    public String getQueryString() {
        return queryString;
    }

    public String getQueryField() {
        return queryField;
    }

    public Page getPage() {
        return page;
    }

    public Sort getSort() {
        return sort;
    }

    public long getPartnerId() {
        return partnerId;
    }

    public Map<String, String> getAdditionalQueryFields() {
        return additionalQueryFields;
    }

    public String getAssetType() {return  assetType;}
}
