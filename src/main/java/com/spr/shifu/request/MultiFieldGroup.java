package com.spr.shifu.request;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * User: rajesh
 * Date: 10/06/14
 * Time: 2:52 AM
 */
public class MultiFieldGroup extends Group {
    private List<Group> groups;

    public MultiFieldGroup() {
        setGroupType(GroupType.MULTI_FIELD);
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void addGroup(Group group) {
        if(groups == null) {
            groups = Lists.newArrayList();
        }
        groups.add(group);
    }

    public List<Projection> getProjections() {
        return groups.get(groups.size()-1).getProjections();
    }

    public void setSort(Sort sort) {
        groups.get(groups.size()-1).setSort(sort);
    }

    public Sort getSort() {
        return groups.get(groups.size()-1).getSort();
    }

    public Page getPage() {
        return groups.get(groups.size()-1).getPage();
    }

    public String getDateHistogramGroupInterval() {
        for (Group group : groups) {
            if(group.getGroupType() == GroupType.DATE_HISTOGRAM) {
                return (String) group.getDetails().get("interval");
            }
        }
        return null;
    }

    public int getDateHistogramIndex() {
        for(int i = 0; i<groups.size(); i++) {
            if(groups.get(i).getGroupType() == GroupType.DATE_HISTOGRAM) {
                return i;
            }
        }
        return -1;
    }

    public Group getGroupByKey(String key) {
        if(key == null) {
            return null;
        }
        for (Group group : groups) {
            if(key.equals(group.getKey())) {
                return group;
            }
        }
        return null;
    }
}


