package com.spr.shifu.request;

import com.google.common.collect.Lists;
import org.apache.commons.collections.MapUtils;

import java.util.*;

public class Request {
    public enum ProjectionDecoration {
        CHANGE,
        PERCENTAGE_CHANGE,
        PERCENTAGE
    }

    private String key = "DEFAULT";

    private TimeFilter timeFilter;

    private TimeFilter previousTimeFilter;

    private String reportingEngine;

    private Query query;

    private List<Filter> filters;

    private List<Filter> postFilters;

    private List<Filter> projectionFilters;

    private List<Group> groupBys;

    private List<Projection> projections;

    private String report;

    private long tzOffset;

    private String timezone;

    private List<Sort> sorts;

    private Page page;

    private Page pageForDocuments;

    private List<Sort> sortsForDocuments;

    private Map<String, String> additional;

    private boolean cacheDisabled;

    private Set<ProjectionDecoration> projectionDecorations = new HashSet<>();

    public Request key(String key) {
        this.key = key;
        return this;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Map<String, String> getAdditional() {
        return additional;
    }

    public String getAdditional(String key) {
        return MapUtils.getString(additional, key);
    }

    public void setAdditional(Map<String, String> additional) {
        this.additional = additional;
    }

    public String getReportingEngine() {
        return reportingEngine;
    }

    public void setReportingEngine(String reportingEngine) {
        this.reportingEngine = reportingEngine;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public TimeFilter getTimeFilter() {
        return timeFilter;
    }

    public void setTimeFilter(TimeFilter timeFilter) {
        this.timeFilter = timeFilter;
    }

    public TimeFilter getPreviousTimeFilter() {
        return previousTimeFilter;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setPreviousTimeFilter(TimeFilter previousTimeFilter) {
        this.previousTimeFilter = previousTimeFilter;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public List<Filter> getPostFilters() {
        return postFilters;
    }

    public void setPostFilters(List<Filter> postFilters) {
        this.postFilters = postFilters;
    }

    public List<Filter> getProjectionFilters() {
        return projectionFilters;
    }

    public void setProjectionFilters(List<Filter> projectionFilters) {
        this.projectionFilters = projectionFilters;
    }

    public List<Group> getGroupBys() {
        return groupBys;
    }

    public void setGroupBys(List<Group> groupBys) {
        this.groupBys = groupBys;
    }

    public List<Projection> getProjections() {
        return projections;
    }

    public void setProjections(List<Projection> projections) {
        this.projections = projections;
    }

    public long getTzOffset() {
        return tzOffset;
    }

    public void setTzOffset(long tzOffset) {
        this.tzOffset = tzOffset;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public boolean isCacheDisabled() {
        return cacheDisabled;
    }

    public void setCacheDisabled(boolean cacheDisabled) {
        this.cacheDisabled = cacheDisabled;
    }

    public List<Sort> getSorts() {
        return sorts;
    }

    public void setSorts(List<Sort> sorts) {
        this.sorts = sorts;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Page getPageForDocuments() {
        return pageForDocuments;
    }

    public void setPageForDocuments(Page pageForDocuments) {
        this.pageForDocuments = pageForDocuments;
    }

    public List<Sort> getSortsForDocuments() {
        return sortsForDocuments;
    }

    public void setSortsForDocuments(List<Sort> sortsForDocuments) {
        this.sortsForDocuments = sortsForDocuments;
    }

    @SuppressWarnings("UnusedDeclaration")
    public Set<ProjectionDecoration> getProjectionDecorations() {
        return projectionDecorations;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setProjectionDecorations(Set<ProjectionDecoration> projectionDecorations) {
        this.projectionDecorations = projectionDecorations;
    }

    public Request timeFilter(TimeFilter timeFilter) {
        this.timeFilter = timeFilter;
        return this;
    }

    @SuppressWarnings("UnusedDeclaration")
    public Request previousTimeFilter(TimeFilter previousTimeFilter) {
        this.previousTimeFilter = previousTimeFilter;
        return this;
    }

    public Request tzOffset(long tzOffset) {
        this.tzOffset = tzOffset;
        return this;
    }

    public Request timezone(String timezone) {
        this.timezone = timezone;
        return this;
    }

    public Request addGroupBy(Group group) {
        if (groupBys == null) {
            groupBys = Lists.newArrayList();
        }
        groupBys.add(group);
        return this;
    }

    public Request report(String report) {
        this.report = report;
        return this;
    }

    public Request reportingEngine(String reportingEngine) {
        this.reportingEngine = reportingEngine;
        return this;
    }

    public Request addProjection(Projection projection) {
        if (projections == null) {
            projections = Lists.newArrayList();
        }
        projections.add(projection);
        return this;
    }

    public Request query(Query query) {
        this.query = query;
        return this;
    }

    public Request addFilter(Filter filter) {
        if (filters == null) {
            filters = Lists.newArrayList();
        }
        filters.add(filter);
        return this;
    }

    @SuppressWarnings("UnusedDeclaration")
    public Request addPostFilter(Filter postFilter) {
        if (postFilters == null) {
            postFilters = Lists.newArrayList();
        }
        postFilters.add(postFilter);
        return this;
    }

    public Request addProjectionFilter(Filter projectionFilter) {
        if (projectionFilters == null) {
            projectionFilters = Lists.newArrayList();
        }
        projectionFilters.add(projectionFilter);
        return this;
    }

    public Request addSort(Sort sort) {
        if (sorts == null) {
            sorts = Lists.newArrayList();
        }
        sorts.add(sort);
        return this;
    }

    public Request page(Page page) {
        this.page = page;
        return this;
    }

    public Request addSortForDocument(Sort sort) {
        if (sortsForDocuments == null) {
            sortsForDocuments = Lists.newArrayList();
        }
        sortsForDocuments.add(sort);
        return this;
    }

    public Request pageForDocument(Page page) {
        this.pageForDocuments = page;
        return this;
    }

    public Request addProjectionDecoration(ProjectionDecoration projectionDecoration) {
        projectionDecorations.add(projectionDecoration);
        return this;
    }

    public Group getGroupByKey(String key) {
        for (Group group : getGroupBys()) {
            if (group.getKey().equals(key)) {
                return group;
            }
        }
        return null;
    }

    public boolean hasEitherOfDecoration(ProjectionDecoration... projectionDecorations) {
        for (ProjectionDecoration projectionDecoration : projectionDecorations) {
            if (this.projectionDecorations.contains(projectionDecoration)) {
                return true;
            }
        }
        return false;
    }

    public String getAdditionalParam(String paramName) {
        if (additional != null) {
            return additional.get(paramName);
        }
        return null;
    }

    public Request addAdditionalParam(String paramName, String value) {
        if (additional == null) {
            additional = new HashMap<>();
        }
        additional.put(paramName, value);
        return this;
    }

    public Request additional(Map<String, String> additional) {
        this.additional = additional;
        return this;
    }
}