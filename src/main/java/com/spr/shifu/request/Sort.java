package com.spr.shifu.request;

import com.google.common.collect.Maps;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Sort {

    public enum Order {
        ASC("asc"),
        DESC("desc");
        private static final Map<String, Order> map;

        static {
            Map<String, Order> tmp = Maps.newHashMap();
            for (Order order : Order.values()) {
                tmp.put(order.getOrder(), order);
            }
            map = Collections.unmodifiableMap(tmp);
        }

        private String order;

        Order(String order) {
            this.order = order;
        }

        public static Order fromStringOrNull(String order) {
            return map.get(order);
        }
        public String getOrder() {
            return order;
        }

    }

    private String key;

    private Order order = Order.DESC;

    private Map<String, Object> additional;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Sort key(String key) {
        this.key = key;
        return this;
    }

    public Sort order(Order order) {
        this.order = order;
        return this;
    }


    public Map<String, Object> getAdditional() {
        return additional;
    }

    public void setAdditional(Map<String, Object> additional) {
        this.additional = additional;
    }

    public Sort addAdditional(String key, Object value) {
        if (this.additional == null) {
            this.additional = new HashMap<>();
        }
        this.additional.put(key, value);
        return this;
    }

    public Object getAdditional(String key) {
        if (this.additional == null) {
            return null;
        }
        return this.additional.get(key);
    }
}
