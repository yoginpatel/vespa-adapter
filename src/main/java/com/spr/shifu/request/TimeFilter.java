package com.spr.shifu.request;

public class TimeFilter {

    private String field;
    private long sinceTime;
    private long untilTime;

    public TimeFilter() {
    }

    public TimeFilter(String field, long sinceTime, long untilTime) {
        this.field = field;
        this.sinceTime = sinceTime;
        this.untilTime = untilTime;
    }

    public String getField() {
        return field;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setField(String field) {
        this.field = field;
    }

    public long getSinceTime() {
        return sinceTime;
    }

    public void setSinceTime(long sinceTime) {
        this.sinceTime = sinceTime;
    }

    public long getUntilTime() {
        return untilTime;
    }

    public void setUntilTime(long untilTime) {
        this.untilTime = untilTime;
    }

    public TimeFilter field(String field) {
        this.field = field;
        return this;
    }

    public TimeFilter sinceTime(long sinceTime) {
        this.sinceTime = sinceTime;
        return this;
    }

    public TimeFilter untilTime(long untilTime) {
        this.untilTime = untilTime;
        return this;
    }
}
