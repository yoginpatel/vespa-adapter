package com.spr.ops.vespa;

import com.spr.shifu.request.Request;
import com.yahoo.search.Query;

/**
 * Date 09/10/17
 * Time 12:54 PM
 *
 * @author yogin
 */
public interface VespaQueryAdapter {

    Query adapt(Request request);
}
