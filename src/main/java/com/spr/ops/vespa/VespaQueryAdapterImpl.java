package com.spr.ops.vespa;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.spr.shifu.*;
import com.spr.shifu.request.*;
import com.yahoo.prelude.query.*;
import com.yahoo.search.Query;
import com.yahoo.search.grouping.GroupingRequest;
import com.yahoo.search.grouping.request.*;
import com.yahoo.search.query.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Date 09/10/17
 * Time 12:54 PM
 *
 * @author yogin
 */
public class VespaQueryAdapterImpl implements VespaQueryAdapter {

    private final Report report;

    public VespaQueryAdapterImpl(Report report) {
        this.report = report;
    }

    @Override
    public Query adapt(Request request) {
        Set<String> fields = projections(request.getProjections());
        Set<String> sources = sources(request.getReport());

        Query query = new Query();
        Model model = query.getModel();

        //projections
        if (CollectionUtils.isNotEmpty(sources)) {
            model.setSources(Joiner.on(",").join(sources));
        }
        if (CollectionUtils.isNotEmpty(fields)) {
            Presentation presentation = query.getPresentation();
            presentation.setSummaryFields(Joiner.on(",").join(fields));
        }

        //filters
        if (CollectionUtils.isNotEmpty(request.getFilters())) {
            QueryTree queryTree = model.getQueryTree();
            CompositeItem root = toCompositeItem(request);
            queryTree.setRoot(root);
        }

        //sorts
        List<Sort> sorts = request.getSorts();
        if (CollectionUtils.isNotEmpty(sorts)) {
            Ranking ranking = query.getRanking();
            List<Sorting.FieldOrder> sortOrders = Lists.newArrayList();

            sorts.forEach(sort -> sortOrders.add(new Sorting.FieldOrder(new Sorting.AttributeSorter(translate(sort.getKey())), order(sort.getOrder()))));
            ranking.setSorting(new Sorting(sortOrders));
        }

        //paging
        Page page = request.getPage();
        if (page != null) {
            query.setOffset(page.getSkip());
            query.setHits(page.getSize());
        }

        //aggregations
        List<Group> groupBys = request.getGroupBys();
        if (CollectionUtils.isNotEmpty(groupBys)) {
            GroupingRequest groupingRequest = GroupingRequest.newInstance(query);
            GroupingOperation rootOperation = new AllOperation();
            groupingRequest.setRootOperation(rootOperation);

            addGroupOperation(groupBys, rootOperation);
        }
        return query;
    }

    private void addGroupOperation(List<Group> groupBys, GroupingOperation rootOperation) {
        if (CollectionUtils.isEmpty(groupBys)) {
            return;
        }

        for (Group groupBy : groupBys) {
            String field = translate(groupBy.getField());

            GroupingOperation groupOperation = null;
            EachOperation eachOperation = new EachOperation();

            Group.GroupType groupType = groupBy.getGroupType();
            switch (groupType) {
                case FIELD:
                case WORD_CLOUD:
                    groupOperation = new AllOperation()
                            .setGroupBy(new AttributeValue(field)).setLabel(groupBy.getKey())
                            .addChild(eachOperation
                                    .addOutput(new CountAggregator().setLabel("count")));

                    break;
                case TIME_OF_DAY:
                case DAY_OF_WEEK:
                case MONTH_OF_YEAR:
                    groupOperation = new AllOperation()
                            .setGroupBy(TimeFunctions.newInstance(timeType(groupType), new AttributeValue(field))).setLabel(groupBy.getKey())
                            .addChild(eachOperation
                                    .addOutput(new CountAggregator().setLabel("count")));
                    break;

                //TODO:
                case FILTER:
                    break;
                case MULTI_FIELD:
                    break;
                case RANGE:
                    break;
                case HISTOGRAM:
                    break;
                case DATE_HISTOGRAM:
                    break;
            }

            List<Projection> projections = groupBy.getProjections();
            if (CollectionUtils.isNotEmpty(projections)) {
                for (Projection projection : projections) {
                    eachOperation.addOutput(outputOperation(projection));
                }
            }

            if (groupOperation != null) {
                rootOperation.addChild(groupOperation);
            }

            List<Group> childrenGroupBys = groupBy.getChildrenGroupBys();
            addGroupOperation(childrenGroupBys, groupOperation);
        }
    }

    private GroupingExpression outputOperation(Projection projection) {
        Projection.AggregateFunction aggregateFunction = projection.getAggregateFunction();
        switch (aggregateFunction) {
            case SUM:
                return new SumAggregator(new AttributeValue(translate(projection.getMeasurement()))).setLabel(projection.getKey());
            case AVG:
                return new AvgAggregator(new AttributeValue(translate(projection.getMeasurement()))).setLabel(projection.getKey());
            case MIN:
                return new MinAggregator(new AttributeValue(translate(projection.getMeasurement()))).setLabel(projection.getKey());
            case MAX:
                return new MaxAggregator(new AttributeValue(translate(projection.getMeasurement()))).setLabel(projection.getKey());
            case COUNT:
                return new CountAggregator().setLabel(projection.getKey());

            //TODO
            case STATS:
                break;
            case EXTERNAL_VALUE:
                break;
            case CARDINALITY:
                break;
            case TOP_HITS:
                break;
            case PERCENTILE:
                break;
        }
        return null;
    }

    private CompositeItem toCompositeItem(Request request) {
        List<Filter> filters = request.getFilters();
        return addToCompositeItem(new AndItem(), filters);
    }

    private CompositeItem addToCompositeItem(CompositeItem root, List<Filter> filters) {
        for (Filter filter : filters) {
            root.addItem(toVespaItem(filter));
        }
        return root;
    }

    private Item toVespaItem(Filter filter) {
        //todo: add validation on values type

        List<Object> values = filter.getValues();
        String field = translate(filter.getField());

        Item item;
        switch (filter.getFilterType()) {
            case AND:
                item = addToCompositeItem(new AndItem(), filter.getFilters());
                break;
            case OR:
                item = addToCompositeItem(new OrItem(), filter.getFilters());
                break;
            case NOT:
                item = addToCompositeItem(new NotItem(), filter.getFilters());
                break;
            case IN:
                if (values.size() == 1) {
                    item = new WordItem(String.valueOf(values.get(0)), field);
                } else {
                    OrItem orItem = new OrItem();
                    for (Object value : values) {
                        orItem.addItem(new WordItem(String.valueOf(value), field));
                    }
                    item = orItem;
                }
                break;
            case GT:
                item = new RangeItem(new Limit(Long.valueOf(String.valueOf(values.get(0))), false), Limit.POSITIVE_INFINITY, field);
                break;
            case GTE:
                item = new RangeItem(new Limit(Long.valueOf(String.valueOf(values.get(0))), true), Limit.POSITIVE_INFINITY, field);
                break;
            case LT:
                item = new RangeItem(Limit.NEGATIVE_INFINITY, new Limit(Long.valueOf(String.valueOf(values.get(0))), false), field);
                break;
            case LTE:
                item = new RangeItem(Limit.NEGATIVE_INFINITY, new Limit(Long.valueOf(String.valueOf(values.get(0))), true), field);
                break;
            case NIN:
                NotItem notItem = new NotItem();
                if (values.size() == 1) {
                    notItem.addItem(new WordItem(String.valueOf(values.get(0)), field));
                } else {
                    OrItem orItem = new OrItem();
                    for (Object value : values) {
                        orItem.addItem(new WordItem(String.valueOf(value), field));
                    }
                    notItem.addItem(orItem);
                }
                item = notItem;
                break;
            case BETWEEN:
                item = new RangeItem(new Limit(Long.valueOf(String.valueOf(values.get(0))), true), new Limit(Long.valueOf(String.valueOf(values.get(1))), true), field);
                break;
            case STARTS_WITH:
                item = new PrefixItem(String.valueOf(values.get(0)), field);
                break;
            case REGEX:
                item = new RegExpItem(field, false, String.valueOf(values.get(0)));
                break;
            case CONTAINS:
                //todo
                item = new WordItem(String.valueOf(values.get(0)), field);
                break;
            case EQUALS:
                //todo
                item = new WordItem(String.valueOf(values.get(0)), field);
                break;
            case FILTER:
                //and?
                item = addToCompositeItem(new AndItem(), filter.getFilters());
                break;
            case SEARCH:
                //TODO
                item = new WordItem(String.valueOf(values.get(0)), field);
                break;
            //TODO
            case DOES_NOT_CONTAIN:
            case EXISTS:
            case LIMIT:
            case MISSING:
            case ADHOC_SEARCH:
            case EXPRESSION:
            case GEO_DISTANCE:
            case NOT_EQUALS:
            case ADVANCED_QUERY:
            default:
                throw new UnsupportedOperationException(filter.getFilterType() + " is not yet supported");
        }

        return item;
    }

    private static Sorting.Order order(Sort.Order order) {
        return Sort.Order.DESC == order ? Sorting.Order.DESCENDING : Sorting.Order.ASCENDING;
    }

    private static TimeFunctions.Type timeType(Group.GroupType groupType) {
        switch (groupType) {
            case TIME_OF_DAY:
                return TimeFunctions.Type.HOUR_OF_DAY;
            case DAY_OF_WEEK:
                return TimeFunctions.Type.DAY_OF_WEEK;
            case MONTH_OF_YEAR:
                return TimeFunctions.Type.MONTH_OF_YEAR;
        }
        throw new UnsupportedOperationException(groupType + " not supported as time function");
    }

    private Set<String> sources(String report) {
        if (report == null) {
            return null;
        }
        return Sets.newHashSet(report.split(",")); //TODO: source from report
    }

    private Set<String> projections(List<Projection> projections) {
        if (CollectionUtils.isEmpty(projections)) {
            return Collections.emptySet();
        }

        Set<String> fields = Sets.newHashSet();
        for (Projection projection : projections) {
            fields.add(translate(projection));
        }
        return fields;
    }

    private String translate(Projection projection) {
        return translate(projection.getMeasurement());
    }

    private String translate(String field) {
        return translateField(field, null);
    }

    private String translateField(String fieldName, Map<String, Object> details) {
        ReportMetadata reportMetadata = report.getMetadata();
        Field field = reportMetadata.getField(fieldName);

        if (reportMetadata.getDateField().equals(fieldName)) {
            return reportMetadata.getDateField();
        } else if (field instanceof Measurement) {
            return report.translateMeasurement(fieldName);
        } else if (field instanceof Dimension) {
            if (MapUtils.isEmpty(details)) {
                return report.translateDimension(fieldName);
            } else {
                return report.translateDimension(fieldName, details);
            }
        }
        return fieldName;
    }
}
