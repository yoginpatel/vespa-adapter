package com.spr.ops.vespa;

import com.spr.shifu.request.Request;

/**
 * TODO: delete this class now. Not needed. it was added before the VespaQueryAdapter was constructed.
 * - test VespaQueryAdapter can adapt all requests and remove this YQL adapter.
 *
 * Date 05/10/17
 * Time 9:54 PM
 *
 * @author yogin
 */
public interface VespaYQLAdapter {
    String adapt(Request request);
}
