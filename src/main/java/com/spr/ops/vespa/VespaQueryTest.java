package com.spr.ops.vespa;

import com.yahoo.prelude.query.AndItem;
import com.yahoo.prelude.query.CompositeItem;
import com.yahoo.prelude.query.NearItem;
import com.yahoo.prelude.query.WordItem;
import com.yahoo.search.Query;
import com.yahoo.search.query.Model;
import com.yahoo.search.query.QueryTree;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Date 06/10/17
 * Time 4:08 PM
 *
 * @author yogin
 */
public class VespaQueryTest {
    public static void main(String[] args) throws UnsupportedEncodingException {
        /*String encoded = encode("select * from sources * where sddocname contains \"tweet\" | all(group(lang) each(output(count())));");
        c("?query="+encoded);*/



        Query query = new Query();

        Model model = query.getModel();

        QueryTree queryTree = model.getQueryTree();
        CompositeItem root = new AndItem();
        queryTree.setRoot(root);

        /*root.addItem(new WordItem("tweet"));
        root.addItem(new WordItem("1234", "userId"));*/

        NearItem nearItem = new NearItem(5);
        nearItem.addItem(new WordItem("hello"));
        nearItem.addItem(new WordItem("world"));

        root.addItem(nearItem);



        System.out.println(query.yqlRepresentation());
    }

    private static String encode(String s) throws UnsupportedEncodingException {
        return URLEncoder.encode(s, "UTF-8");
    }
}
