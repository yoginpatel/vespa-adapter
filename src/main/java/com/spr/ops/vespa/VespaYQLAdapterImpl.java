package com.spr.ops.vespa;

import com.google.common.collect.Sets;
import com.spr.shifu.request.*;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Date 05/10/17
 * Time 9:55 PM
 *
 * @author yogin
 */
public class VespaYQLAdapterImpl implements VespaYQLAdapter {
    @Override
    public String adapt(Request request) {
        Set<String> fields = projections(request.getProjections());
        Set<String> sources = sources(request.getReport());

        StringBuilder yql = new StringBuilder("select ");
        if (CollectionUtils.isEmpty(fields)) {
            yql.append("*");
        } else {
            commaSeparated(yql, fields);
        }

        yql.append(" from ");
        if (sources.isEmpty()) {
            yql.append("sources *");
        } else {
            if (sources.size() > 1) {
                yql.append("sources ");
            }
            commaSeparated(yql, sources);
        }

        appendFilters(yql, request.getFilters(), " and ");
        appendSorts(yql, request.getSorts());
        appendPaging(yql, request.getPage());

        yql.append(';');
        return yql.toString();
    }

    private void appendFilters(StringBuilder yql, List<Filter> filters, String operand) {
        if (CollectionUtils.isEmpty(filters)) {
            return;
        }

        yql.append(" where ");
        boolean append = false;
        for (Filter filter : filters) {
            if (append) {
                yql.append(operand);
            }
            append = true;
            yql.append(criteria(filter));
        }
    }

    private void appendSorts(StringBuilder yql, List<Sort> sorts) {
        if (CollectionUtils.isEmpty(sorts)) {
            return;
        }

        yql.append(" order by ");
        boolean append = false;
        for (Sort sort : sorts) {
            if (append) {
                yql.append(", ");
            }
            append = true;
            yql.append(translate(sort.getKey()));
            Sort.Order order = sort.getOrder();
            if (Sort.Order.DESC == order) {
                yql.append(" desc");
            }
            //default is asc
        }
    }

    private void appendPaging(StringBuilder yql, Page page) {
        if (page == null) {
            return;
        }
        yql.append(" limit ").append(page.getSize()).append(" offset ").append(page.getSkip());
    }

    private String criteria(Filter filter) {
        Filter.FilterType filterType = filter.getFilterType();
        List<Filter> subFilters = filter.getFilters();
        String field = translate(filter.getField());
        List<Object> values = filter.getValues();

        StringBuilder sb = new StringBuilder("(");
        switch (filterType) {
            case AND:
                appendFilters(sb, subFilters, " and ");
                break;
            case OR:
                appendFilters(sb, subFilters, " or ");
                break;
            case NOT:
                break;
            case IN:
                if (values.size() == 0) {
                    throw new RuntimeException("IN filter has no values in filter: " + field);
                }
                sb.append(field);
                if (values.size() == 1) {
                    sb.append(" contains ");
                    sb.append("\"").append(values.get(0)).append("\"");
                } else {
                    sb.append(" in ");
                    for (Object value : values) {
                        sb.append("\"").append(value).append("\"");
                    }
                }
                break;
            case GT:
            case GTE:
            case LT:
            case LTE:
            case EQUALS:
                appendNumeric(sb, field, values, filterType);
                break;
            case NIN:
                break;
            case BETWEEN:
                validateOperands(filterType, field, values, 2);
                sb.append("range(").append(field).append(", ").append(values.get(0)).append(", ").append(values.get(1)).append(")");
                break;
            case STARTS_WITH:
                validateOperands(filterType, field, values, 1);
                sb.append(field).append(" matches \"^").append(values.get(0)).append("\"");
                break;
            case CONTAINS:
                validateOperands(filterType, field, values, 1);
                sb.append(field).append(" contains ").append("\"").append(values.get(0)).append("\"");
                break;
            case DOES_NOT_CONTAIN:
                throw new UnsupportedOperationException("DOES_NOT_CONTAIN");
            case FILTER:
                appendFilters(sb, subFilters, " and ");
                break;
            case EXISTS:
                break;
            case REGEX:
                validateOperands(filterType, field, values, 1);
                sb.append(field).append(" matches \"").append(values.get(0)).append("\"");
                break;
            case LIMIT:
                break;
            case SEARCH:
                validateOperands(filterType, field, values, 1);
                sb.append("default").append(" contains ").append("\"").append(values.get(0)).append("\"");
                break;
            case MISSING:
                break;
            case ADHOC_SEARCH:
                break;
            case EXPRESSION:
                break;
            case GEO_DISTANCE:
                break;
            case NOT_EQUALS:
                break;
            case ADVANCED_QUERY:
                break;
        }
        sb.append(")");
        return sb.toString();
    }

    private void appendNumeric(StringBuilder sb, String field, List<Object> values, Filter.FilterType filterType) {
        validateOperands(filterType, field, values, 1);
        String operator;
        switch (filterType) {
            case GT:
                operator = " > ";
                break;
            case GTE:
                operator = " >= ";
                break;
            case LT:
                operator = " < ";
                break;
            case LTE:
                operator = " <= ";
                break;
            case EQUALS:
                operator = " = ";
                break;
            default: throw new UnsupportedOperationException(filterType + " not supported for range");
        }
        sb.append(field).append(operator).append(values.get(0));
    }

    private void validateOperands(Filter.FilterType filterType, String field, List<Object> values, int expectedSize) {
        if (values.size() != expectedSize) {
            throw new RuntimeException(filterType + " filter has no valid values in filter: " + field);
        }
    }

    private Set<String> sources(String report) {
        if (report == null) {
            return null;
        }
        return Sets.newHashSet(report.split(","));
    }

    private void commaSeparated(StringBuilder yql, Set<String> fields) {
        int initLen = yql.length();
        for (String field : fields) {
            if (yql.length() > initLen) {
                yql.append(", ");
            }
            yql.append(field);
        }
    }

    private Set<String> projections(List<Projection> projections) {
        if (CollectionUtils.isEmpty(projections)) {
            return Collections.emptySet();
        }

        Set<String> fields = Sets.newHashSet();
        for (Projection projection : projections) {
            fields.add(translate(projection));
        }
        return fields;
    }

    private String translate(Projection projection) {
        return projection.getKey(); //report metadata here
    }

    private String translate(String field) {
        return field; //report metadata here
    }
}
