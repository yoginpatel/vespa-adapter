- on grouping:
    - Date Histo, Histo (interval based aggregations)
    - Custom aggregations of our ES Plugin, Calculated field aggregation (script based)
    - Group Filter (filter on grouped data) - FilterAggregationBuilder of ES
    - Multi Field Group aggregator. (es plugin)
    - Projection Filters

- on indexing:
    - analyzers support. affects search and matching. For ES we use many different and custom analyzers of lucene
    - dynamic field mapping in search definitions
    - Time sharding is "g=" in id notation? http://docs.vespa.ai/documentation/documents.html (need this for timeFilter adaption)